<?php

namespace Drupal\Tests\views_xml_backend\Unit\Plugin\views\filter;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\views_xml_backend\Unit\ViewsXmlBackendTestBase;
use Drupal\views_xml_backend\Plugin\views\filter\Standard;

/**
 * @coversDefaultClass \Drupal\views_xml_backend\Plugin\views\filter\Standard
 * @group views_xml_backend
 */
class StandardTest extends ViewsXmlBackendTestBase {

  /**
   * The mocked database connection.
   *
   * @var \Drupal\Core\Database\Connection|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    $this->connection = $this->createMock(Connection::class);
  }

  /**
   * @covers ::__toString
   */
  public function testToString() {
    $configuration = [];
    $plugin_id = 'standard';
    $plugin_definition = [];

    $plugin = new Standard($configuration, $plugin_id, $plugin_definition, $this->connection);

    $options = ['xpath_selector' => 'xpath_query'];

    $plugin->init($this->getMockedView(), $this->getMockedDisplay(), $options);

    $plugin->operator = '=';
    $plugin->value = 'foo';
    $this->assertSame("xpath_query = 'foo'", (string) $plugin);

    $plugin->operator = '!=';
    $this->assertSame("xpath_query != 'foo'", (string) $plugin);

    $plugin->operator = 'contains';
    $this->assertSame("contains(xpath_query, 'foo')", (string) $plugin);

    $plugin->operator = '!contains';
    $this->assertSame("not(contains(xpath_query, 'foo'))", (string) $plugin);
  }

}
