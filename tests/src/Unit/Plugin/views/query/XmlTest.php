<?php

namespace Drupal\Tests\views_xml_backend\Unit\Plugin\views\query {

  use Drupal\Core\Cache\NullBackend;
  use Drupal\Core\Extension\ModuleHandlerInterface;
  use Drupal\Core\File\FileSystemInterface;
  use Drupal\Core\Messenger\MessengerInterface;
  use Drupal\Core\Site\Settings;
  use Drupal\Tests\views_xml_backend\Unit\ViewsXmlBackendTestBase;
  use Drupal\views_xml_backend\Plugin\views\query\Xml;
  use GuzzleHttp\Client;
  use GuzzleHttp\Handler\MockHandler;
  use GuzzleHttp\HandlerStack;
  use GuzzleHttp\Psr7\Response;
  use org\bovigo\vfs\vfsStream;
  use Psr\Log\NullLogger;

  /**
   * @coversDefaultClass \Drupal\views_xml_backend\Plugin\views\query\Xml
   * @group views_xml_backend
   */
  class XmlTest extends ViewsXmlBackendTestBase {

    /**
     * The XML query object.
     *
     * @var \Drupal\views_xml_backend\Plugin\views\query\Xml
     */
    protected $query;

    /**
     * The file system service.
     *
     * @var \Drupal\Core\File\FileSystemInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $fileSystem;

    /**
     * The module handler service.
     *
     * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $moduleHandler;

    /**
     * The HTTP client.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * The messenger service.
     *
     * @var \Drupal\Core\Messenger\MessengerInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $messenger;

    protected $testMessages = [];

    /**
     * {@inheritdoc}
     */
    public function setUp(): void {
      parent::setUp();

      if (!defined('FILE_MODIFY_PERMISSIONS')) {
        define('FILE_MODIFY_PERMISSIONS', 2);
      }
      if (!defined('FILE_CREATE_DIRECTORY')) {
        define('FILE_CREATE_DIRECTORY', 1);
      }
      if (!defined('FILE_EXISTS_REPLACE')) {
        define('FILE_EXISTS_REPLACE', 1);
      }

      vfsStream::setup('vxb');
      new Settings(['views_xml_backend_cache_directory' => 'vfs://vxb/filecache']);

      $this->client = new Client(['handler' => HandlerStack::create(new MockHandler([]))]);
      $this->fileSystem = $this->createMock(FileSystemInterface::class);
      $this->messenger = $this->createMock(MessengerInterface::class);

      $this->query = new Xml(
        [],
        'xml',
        [],
        $this->client,
        new NullBackend('bin'),
        new NullLogger(),
        $this->fileSystem,
        $this->messenger
      );

      $this->query->setStringTranslation($this->getStringTranslationStub());
    }

    public function testExecuteWithEmptyRowXpathDisplaysMessage() {
      $view = $this->getMockedView();
      $display = $this->getMockedDisplay();

      // Configure the query with an empty row XPath.
      $this->query->options['row_xpath'] = '';

      $messageCalled = FALSE;
      $this->messenger->expects($this->once())
        ->method('addMessage')
        ->willReturnCallback(function ($message, $type) use (&$messageCalled) {
          $messageCalled = TRUE;
          $this->assertEquals('warning', $type);
          $this->assertEquals('Please configure the query settings.', $message->render());
        });

      $this->query->init($view, $display);
      $this->query->build($view);
      $this->query->execute($view);

      $this->assertTrue($messageCalled, 'addMessage was not called');
      $this->assertEmpty($view->result, 'View result should be empty');
    }

    public function testExecuteWithValidRowXpath() {
      $view = $this->getMockedView();
      $display = $this->getMockedDisplay();

      // Configure the query with a valid row XPath.
      $options = [
        'row_xpath' => '/foo/bar',
        'xml_file' => 'http://example.com/test.xml',
      ];

      // Mock the HTTP client to return a sample XML.
      $sampleXml = '<foo><bar><value>1</value></bar><bar><value>2</value></bar></foo>';
      $mock = new MockHandler([
        new Response(200, [], $sampleXml),
      ]);
      $handler = HandlerStack::create($mock);
      $client = new Client(['handler' => $handler]);

      // Create a new Xml query object with the mocked client.
      $query = new Xml(
        [],
        'xml',
        [],
        $client,
        new NullBackend('bin'),
        new NullLogger(),
        $this->fileSystem,
        $this->messenger
      );

      $query->setStringTranslation($this->getStringTranslationStub());

      $this->messenger->expects($this->never())
        ->method('addMessage');

      $query->init($view, $display, $options);
      $query->build($view);
      $query->execute($view);

      $this->assertNotEmpty($view->result, 'View result should not be empty');
      $this->assertCount(2, $view->result, 'View result should contain 2 items');
    }

    public function testBasicXmlParsing() {
      $xml = <<<XML
<foo>
  <bar><value>1</value></bar>
  <bar><value>2</value></bar>
  <bar><value>3</value></bar>
  <bar><value>4</value></bar>
</foo>
XML;
      $mock = new MockHandler([
        new Response(200, [], $xml),
      ]);

      $handler = HandlerStack::create($mock);
      $this->client = new Client(['handler' => $handler]);

      $query = $this->getNewQueryObject([
        'xml_file' => 'http://example.com',
        'row_xpath' => '/foo/bar',
      ]);

      $view = $this->getMockedView();

      // Fake the fields.
      $view->field['field_1'] = (object) ['options' => ['xpath_selector' => 'value']];

      // Expect that addMessage is never called (i.e., no errors)
      $this->messenger->expects($this->never())
        ->method('addMessage');

      $query->build($view);
      $query->execute($view);

      $this->assertCount(4, $view->result);

      foreach ($view->result as $index => $row) {
        $this->assertSame($index, $row->index);
        $value = (string) ($index + 1);
        $this->assertSame([0 => $value], $row->field_1);
      }
    }

    public function testAddExtraFieldsWorks() {
      $xml = <<<XML
<foo>
  <bar><value>1</value></bar>
  <bar><value>2</value></bar>
  <bar><value>3</value></bar>
  <bar><value>4</value></bar>
</foo>
XML;
      $mock = new MockHandler([
        new Response(200, [], $xml),
      ]);

      $handler = HandlerStack::create($mock);
      $this->client = new Client(['handler' => $handler]);

      $query = $this->getNewQueryObject([
        'xml_file' => 'http://example.com',
        'row_xpath' => '/foo/bar',
      ]);

      $view = $this->getMockedView();

      $query->addField('field_1', 'value');

      // Expect that addMessage is never called (i.e., no errors)
      $this->messenger->expects($this->never())
        ->method('addMessage');

      $query->build($view);
      $query->execute($view);

      $this->assertCount(4, $view->result);

      foreach ($view->result as $index => $row) {
        $this->assertSame($index, $row->index);
        $value = (string) ($index + 1);
        $this->assertSame([0 => $value], $row->field_1);
      }
    }

    protected function getNewQueryObject(array $options) {
      $query = new Xml(
        [],
        '',
        [],
        $this->client,
        new NullBackend('bin'),
        new NullLogger(),
        $this->fileSystem,
        $this->messenger
      );

      $query->setStringTranslation($this->getStringTranslationStub());
      $query->init($this->getMockedView(), $this->getMockedDisplay(), $options);

      return $query;
    }

  }
}

namespace {
  if (!function_exists('file_prepare_directory')) {

    function file_prepare_directory(&$directory) {
      return mkdir($directory);
    }

  }

  if (!function_exists('file_unmanaged_save_data')) {

    function file_unmanaged_save_data($data, $destination) {
      file_put_contents($destination, $data);
      return $destination;
    }

  }
}
