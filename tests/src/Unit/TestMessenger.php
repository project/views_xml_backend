<?php

namespace Drupal\Tests\views_xml_backend\Unit;

use Drupal\Core\Messenger\MessengerInterface;

/**
 * Mock class to replace the messenger service in unit tests.
 */
class TestMessenger implements MessengerInterface {

  /**
   * Array of messages.
   *
   * @var array
   */
  protected $messages = NULL;

  /**
   * {@inheritdoc}
   */
  public function addMessage($message, $type = self::TYPE_STATUS, $repeat = FALSE) {
    if (!empty($message)) {
      $this->messages[$type] = $this->messages[$type] ?? [];
      if ($repeat || !in_array($message, $this->messages[$type])) {
        $this->messages[$type][] = $message;
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessages($type = NULL, $clear = TRUE) {
    if ($type === NULL) {
      $messages = $this->messages;
      if ($clear) {
        $this->deleteAll();
      }
      return $messages;
    }

    if (isset($this->messages[$type])) {
      $messages = $this->messages[$type];
      if ($clear) {
        unset($this->messages[$type]);
      }
      return $messages;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function addStatus($message, $repeat = FALSE) {
    return $this->addMessage($message, static::TYPE_STATUS, $repeat);
  }

  /**
   * {@inheritdoc}
   */
  public function addError($message, $repeat = FALSE) {
    return $this->addMessage($message, static::TYPE_ERROR, $repeat);
  }

  /**
   * {@inheritdoc}
   */
  public function addWarning($message, $repeat = FALSE) {
    return $this->addMessage($message, static::TYPE_WARNING, $repeat);
  }

  /**
   * {@inheritdoc}
   */
  public function all() {
    return $this->messages;
  }

  /**
   * {@inheritdoc}
   */
  public function messagesByType($type) {
    if (!empty($type)) {
      return $this->messages[$type] ?? [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    $this->messages = [];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteByType($type) {
    unset($this->messages[$type]);
    return $this;
  }

}
