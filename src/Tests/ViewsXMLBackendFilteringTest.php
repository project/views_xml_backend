<?php

namespace Drupal\Tests\views_xml_backend\Functional;

use Drupal\views\Entity\View;
use Drupal\views_xml_backend\Tests\ViewsXMLBackendBase;

/**
 * Tests filtering functions from the Views XML Backend module.
 *
 * @group views_xml_backend
 */
class ViewsXMLBackendFilteringTest extends ViewsXMLBackendBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views, $modules);

    // Set up the necessary variables.
    $this->setupViewsVariables();

    // Create a basic XML Backend view for testing.
    $this->createXmlBackendView();
  }

  /**
   * Creates a basic XML Backend view for testing.
   */
  protected function createXmlBackendView() {
    $view = View::create([
      'label' => $this->viewsXmlBackendTitle,
      'id' => $this->viewsXmlBackendViewId,
      'base_table' => 'views_xml_backend',
      'display' => [
        'default' => [
          'display_plugin' => 'default',
          'id' => 'default',
          'display_options' => [
            'query' => [
              'type' => 'views_xml_backend',
              'options' => [
                'xml_file' => $this->viewsXmlBackendFile,
                'row_xpath' => '/project/releases/release',
              ],
            ],
          ],
        ],
      ],
    ]);
    $view->save();

    $this->assertTrue(View::load($this->viewsXmlBackendViewId) !== NULL, 'XML Backend view has been created.');
  }

  /**
   * Tests Views XML Backend View filtering.
   */
  public function testFilteringViewsXmlBackend() {
    // Check add filtering ability.
    $this->drupalGet("admin/structure/views/nojs/add-handler/{$this->viewsXmlBackendViewId}/default/filter");
    $this->assertSession()->statusCodeEquals(200);

    $this->submitForm(['name[views_xml_backend.text]' => 'views_xml_backend.text'], 'Add and configure filter criteria');

    $this->assertSession()->fieldExists('options[xpath_selector]');

    $edit = [
      'options[xpath_selector]' => 'version_major',
      'options[operator]' => '!=',
      'options[value]' => '3',
    ];
    $this->submitForm($edit, 'Apply');

    $this->drupalGet("admin/structure/views/nojs/handler/{$this->viewsXmlBackendViewId}/default/filter/text");
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldValueEquals('options[xpath_selector]', 'version_major');
    $this->assertSession()->fieldValueEquals('options[operator]', '!=');
    $this->assertSession()->fieldValueEquals('options[value]', '3');
  }

}
