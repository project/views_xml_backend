<?php

namespace Drupal\views_xml_backend\Tests;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\views_ui\Functional\UITestBase;
use Drupal\views\Views;

/**
 * Provides supporting functions for testing the Views XML Backend module.
 */
abstract class ViewsXMLBackendBase extends UITestBase {
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  use StringTranslationTrait;

  protected $strictConfigSchema = FALSE;

  /**
   * Modules to enable for this test.
   *
   * @var string[]
   */
  public static $modules = [
    'views',
    'views_ui',
    'views_xml_backend',
  ];

  /**
   * The administrator account to use for the tests.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $viewsXmlBackendUser;

  /**
   * Views XML Backend field id.
   *
   * @var string
   */
  protected $viewsXmlBackendViewFieldId;

  /**
   * Views XML Backend field name.
   *
   * @var string
   */
  protected $viewsXmlBackendViewFieldName;

  /**
   * Views XML Backend field value.
   *
   * @var string
   */
  protected $viewsXmlBackendViewValue;

  /**
   * Views XML Backend base view title.
   *
   * @var string
   */
  protected $viewsXmlBackendTitle;

  /**
   * Views XML Backend base view id.
   *
   * @var string
   */
  protected $viewsXmlBackendViewId;

  /**
   * Views XML Backend base view admin add path.
   *
   * @var string
   */
  protected $viewsXmlBackendViewAddPath;

  /**
   * Views XML Backend base view admin edit path.
   *
   * @var string
   */
  protected $viewsXmlBackendViewEditPath;

  /**
   * Views XML Backend base view admin query path.
   *
   * @var string
   */
  protected $viewsXmlBackendViewQueryPath;

  /**
   * Views XML Backend base view xml file.
   *
   * @var string
   */
  protected $viewsXmlBackendFile;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views, $modules);

    $permissions = [
      'administer views',
      'administer modules',
      'access administration pages',
      'view the administration theme',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);

    $this->viewsXMLBackendViewId = 'test_xml_backend_view';
    $this->viewsXMLBackendTitle = 'Test XML Backend View';
    $this->viewsXMLBackendFile = 'https://updates.drupal.org/release-history/views/7.x';

    $this->assertTrue(TRUE, 'User permissions: ' . implode(', ', $permissions));
  }

  /**
   * Creates a valid test User with supplied permissions.
   *
   * @param array $permissions
   *   Permissions user should have.
   *
   * @return \Drupal\Core\Session\AccountInterface|false
   *   A user account interface or FALSE if fails
   */
  private function createTestUser(array $permissions = []) {
    return $this->drupalCreateUser($permissions);
  }

  /**
   * Provides settings used for creating and managing Views XML Backend.
   *
   * @return array
   *   An array of settings.
   */
  private function setupViewsXmlBackendVariables() {
    return [
      'field_id' => 'edit-show-wizard-key',
      'field_name' => 'show[wizard_key]',
      'value' => 'standard:views_xml_backend',
      'file' => 'https://updates.drupal.org/release-history/views/7.x',
    ];
  }

  /**
   * Provides variables used generally for creating and managing Views.
   */
  protected function setupViewsVariables() {
    $settings = $this->setupViewsXmlBackendVariables();
    $this->viewsXmlBackendViewFieldId = $settings['field_id'];
    $this->viewsXmlBackendViewFieldName = $settings['field_name'];
    $this->viewsXmlBackendViewValue = $settings['value'];
    $this->viewsXmlBackendFile = $settings['file'];
    $this->viewsXmlBackendViewId = strtolower($this->randomMachineName(16));
    $this->viewsXmlBackendTitle = $this->randomMachineName(16);
    $this->viewsXmlBackendViewAddPath = '/admin/structure/views/add';
    $this->viewsXmlBackendViewEditPath = "/admin/structure/views/view/{$this->viewsXmlBackendViewId}/edit/default";
    $this->viewsXmlBackendViewQueryPath = "admin/structure/views/nojs/display/{$this->viewsXmlBackendViewId}/default/query";
    // $path_to_test_file = drupal_get_path('module', "views_xml_backend");
  }

  /**
   * Adds and verifies the Views XML Backend option during new Views creation.
   */
  protected function addXmlBackendView() {
    $this->drupalGet('admin/structure/views/add');
    $settings = $this->setupViewsXmlBackendVariables();
    $this->assertSession()->selectExists($settings['field_id']);
    $this->assertSession()->optionExists($settings['field_id'], $settings['value']);
    $this->assertSession()->pageTextContains('XML');
  }

  /**
   * Adds and verifies that a new Views XML Backend View can be created.
   */
  protected function addMinimalXmlBackendView() {
    $this->setupViewsVariables();

    $this->drupalGet($this->viewsXmlBackendViewAddPath);

    $edit = [
      $this->viewsXmlBackendViewFieldName => $this->viewsXmlBackendViewValue,
    ];
    $this->submitForm($edit, 'Update "Show" choice');

    $this->assertSession()->optionExists('edit-show-wizard-key', $this->viewsXmlBackendViewValue);
    $select = $this->assertSession()->selectExists('edit-show-wizard-key');
    $this->assertEquals($this->viewsXmlBackendViewValue, $select->getValue(), 'The correct option is selected.');

    $edit = [
      'label' => $this->viewsXmlBackendTitle,
      'id' => $this->viewsXmlBackendViewId,
      'description' => $this->randomMachineName(16),
      $this->viewsXmlBackendViewFieldName => $this->viewsXmlBackendViewValue,
    ];
    $this->submitForm($edit, 'Save and edit');

    $this->assertSession()->pageTextContains("The view {$this->viewsXmlBackendTitle} has been saved");
  }

  /**
   * Adds and verifies a new Views XML Backend View with specific settings.
   */
  protected function addStandardXmlBackendView() {
    $this->drupalGet('admin/structure/views/add');
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'label' => $this->viewsXMLBackendTitle,
      'id' => $this->viewsXMLBackendViewId,
      'show[wizard_key]' => 'standard:views_xml_backend',
      'page[create]' => 1,
      'page[title]' => $this->viewsXMLBackendTitle,
      'page[path]' => $this->viewsXMLBackendViewId,
    ];

    $this->submitForm($edit, 'Save and edit');
    $this->assertSession()->statusCodeEquals(200);

    // Debug: Check the current page content after form submission.
    $this->assertTrue(TRUE, 'Page content after form submission: ' . substr($this->getSession()->getPage()->getContent(), 0, 1000) . '...');

    // Debug: Check if the view was created.
    $view = Views::getView($this->viewsXMLBackendViewId);
    if (!$view) {
      $this->assertTrue(FALSE, "Failed to create view {$this->viewsXMLBackendViewId}");
      return;
    }

    $this->assertTrue(TRUE, "View {$this->viewsXMLBackendViewId} created successfully");

    // Configure the view with XML backend settings.
    $this->drupalGet("admin/structure/views/nojs/display/{$this->viewsXMLBackendViewId}/default/query");
    $this->assertSession()->statusCodeEquals(200);

    // Debug: Check the current page content before submitting XML settings.
    $this->assertTrue(TRUE, 'Page content before XML settings: ' . substr($this->getSession()->getPage()->getContent(), 0, 1000) . '...');

    $xml_setting = [
      'query[options][xml_file]' => $this->viewsXMLBackendFile,
      'query[options][row_xpath]' => "/project/releases/release",
    ];
    $this->submitForm($xml_setting, 'Apply');

    // Debug: Check the current page content after applying XML settings.
    $this->assertTrue(TRUE, 'Page content after applying XML settings: ' . substr($this->getSession()->getPage()->getContent(), 0, 1000) . '...');

    $this->submitForm([], 'Save');

    // Debug: Verify that the view exists after all operations.
    $final_view = Views::getView($this->viewsXMLBackendViewId);
    $this->assertTrue($final_view !== NULL, "Final check: View {$this->viewsXMLBackendViewId} exists");
  }

  /**
   * Navigate within the Views Pager.
   */
  protected function navigateViewsPager($pager_path) {
    $content = $this->content;
    $drupal_settings = $this->drupalSettings;
    $ajax_settings = [
      'wrapper' => 'views-preview-wrapper',
      'method' => 'replaceWith',
    ];
    $url = $this->getAbsoluteUrl($pager_path);
    $post = ['js' => 'true'] + $this->getAjaxPageStatePostData();
    $result = Json::decode($this->drupalPost($url, 'application/vnd.drupal-ajax', $post));
    if (!empty($result)) {
      $this->drupalProcessAjaxResponse($content, $result, $ajax_settings, $drupal_settings);
    }
  }

}
