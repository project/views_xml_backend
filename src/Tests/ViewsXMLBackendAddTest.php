<?php

namespace Drupal\views_xml_backend\Tests;

/**
 * Tests basic functions from the Views XML Backend module.
 *
 * @group views_xml_backend
 */
class ViewsXMLBackendAddTest extends ViewsXMLBackendBase {

  /**
   * Tests Views XML Backend option appears in new View admin page.
   */
  public function testAddViewViewsXmlBackend() {
    $this->addXmlBackendView();
  }

  /**
   * Tests new Views XML Backend View can be created.
   */
  public function testAddMinimalViewViewsXmlBackend() {
    $this->addMinimalXmlBackendView();
  }

  /**
   * Tests Views XML Backend View can set Query Settings for XML source and set existing/new XML fields.
   */
  public function testAddStandardViewsXmlBackend() {
    $this->addStandardXmlBackendView();
  }

}
