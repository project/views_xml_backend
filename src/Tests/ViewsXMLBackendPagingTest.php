<?php

namespace Drupal\views_xml_backend\Tests;

/**
 * Tests paging functions from the Views XML Backend module.
 *
 * @group views_xml_backend
 */
class ViewsXMLBackendPagingTest extends ViewsXMLBackendBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp();

    // Create a basic XML Backend view.
    $this->addStandardXMLBackendView();
  }

  /**
   * Tests Views XML Backend View paging.
   */
  public function testPagingViewsXmlBackend() {
    $edit_url = "admin/structure/views/view/{$this->viewsXMLBackendViewId}/edit";
    $this->drupalGet($edit_url);

    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldValueEquals('query[options][xml_file]', $this->viewsXMLBackendFile);
    $this->assertSession()->fieldValueEquals('query[options][row_xpath]', '/project/releases/release');

    $this->submitForm([], 'Update preview');

    $elements = $this->xpath('//div[@class = "view-content"]/div[contains(@class, "views-row")]');
    $this->assertNotEmpty($elements, 'View content rows found.');
    $this->assertCount(10, $elements, 'Expected 10 items per page.');

    $this->assertSession()->elementExists('css', 'nav.pager');
    $pager_items = $this->xpath('//ul[contains(@class, "pager__items")]/li');
    $this->assertNotEmpty($pager_items, 'Pager elements found.');

    // Verify elements and links to pages.
    $this->assertStringContainsString('is-active', $pager_items[0]->getAttribute('class'), 'Element for current page has .is-active class.');
    $this->assertNotEmpty($pager_items[0]->find('css', 'a'), 'Element for current page has link.');
    $this->assertStringContainsString('pager__item', $pager_items[1]->getAttribute('class'), 'Element for page 2 has .pager__item class.');
    $this->assertNotEmpty($pager_items[1]->find('css', 'a'), 'Link to page 2 found.');

    // Navigate to next page.
    $next_link = $this->xpath('//li[contains(@class, "pager__item--next")]/a');
    $this->assertNotEmpty($next_link, 'Next page link found.');
    $this->clickLink($next_link[0]->getText());

    $elements = $this->xpath('//div[@class = "view-content"]/div[contains(@class, "views-row")]');
    $this->assertCount(10, $elements, 'Expected 10 items on the second page.');

    // Navigate to previous page.
    $prev_link = $this->xpath('//li[contains(@class, "pager__item--previous")]/a');
    $this->assertNotEmpty($prev_link, 'Previous page link found.');
    $this->clickLink($prev_link[0]->getText());

    $elements = $this->xpath('//div[@class = "view-content"]/div[contains(@class, "views-row")]');
    $this->assertCount(10, $elements, 'Expected 10 items back on the first page.');
  }

}
